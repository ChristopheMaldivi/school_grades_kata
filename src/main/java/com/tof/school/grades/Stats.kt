package com.tof.school.grades

import kotlin.math.pow

object Stats {
    fun mean(values: List<StatValue>): Double {
        return values.map { it.value() }.average()
    }

    fun standardDeviation(values: List<StatValue>): Double {
        val mean = mean(values)
        return values.sumOf { (it.value() - mean).pow(2.0) }
            .div(values.size)
            .pow(0.5)
    }

    fun weightedMeanRaw(multipleValues: List<List<StatValue>>): Double {
        return multipleValues.flatten().sumOf { it.value() }.toDouble()
            .div(multipleValues.sumOf { it.size })
    }

    fun weightedMean(weightsAndMean: List<Pair<Int, Double>>): Double {
        return weightsAndMean.sumOf { it.first * it.second }
            .div(weightsAndMean.sumOf { it.first })
    }

    fun weightedStandardDeviationRaw(multipleValues: List<List<StatValue>>): Double {
        val mean = weightedMeanRaw(multipleValues)
        return multipleValues.sumOf { it.size * (mean(it) - mean).pow(2.0) }
            .div(multipleValues.sumOf { it.size })
            .pow(0.5)
    }

    fun weightedStandardDeviation(weightsAndMean: List<Pair<Int, Double>>): Double {
        val mean = weightedMean(weightsAndMean)
        return weightsAndMean.sumOf { it.first * (it.second - mean).pow(2.0) }
            .div(weightsAndMean.sumOf { it.first })
            .pow(0.5)
    }
}

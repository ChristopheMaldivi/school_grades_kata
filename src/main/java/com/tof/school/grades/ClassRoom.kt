package com.tof.school.grades

class ClassRoom(val name: String, vararg val grades: Grade) {
    fun stats(): MeanAndStd {
        return MeanAndStd(
            GradesStats.mean(grades.toList()),
            GradesStats.standardDeviation(grades.toList())
        )
    }

    fun display(): String {
        return "\t\tClassRoom $name - mean(${stats().mean} std(${stats().std})\n"
    }
}

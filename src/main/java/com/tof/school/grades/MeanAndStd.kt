package com.tof.school.grades

data class MeanAndStd(val mean: Double, val std: Double)

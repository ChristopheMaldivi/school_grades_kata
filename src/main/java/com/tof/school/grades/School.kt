package com.tof.school.grades

class School(val name: String, vararg val classRooms: ClassRoom) {
    fun stats(): MeanAndStd {
        val weightsAndMean = classRooms.map { Pair(it.grades.size, it.stats().mean) }
        val mean = Stats.weightedMean(weightsAndMean)
        val std = Stats.weightedStandardDeviation(weightsAndMean)
        return MeanAndStd(mean, std)
    }

    fun display(): String {
        var str = "\tSchool $name - mean(${stats().mean} std(${stats().std})\n"
        classRooms.forEach {
            str += it.display()
        }
        return str
    }
}

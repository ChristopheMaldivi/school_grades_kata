package com.tof.school.grades

interface StatValue {
    fun value(): Int
}

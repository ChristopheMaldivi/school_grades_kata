package com.tof.school.grades

object GradesStats {
    fun mean(grades: List<Grade>) = Stats.mean(grades)
    fun standardDeviation(grades: List<Grade>) = Stats.standardDeviation(grades)

    fun weightedMean(classRoomsGrades: List<List<Grade>>) = Stats.weightedMeanRaw(classRoomsGrades)
    fun weightedStandardDeviation(classRoomsGrades: List<List<Grade>>) = Stats.weightedStandardDeviationRaw(classRoomsGrades)
}

package com.tof.school.grades

class Town(val name: String, vararg val schools: School) {
    fun stats(): MeanAndStd {
        val weightsAndMean = schools.map { Pair(it.classRooms.size, it.stats().mean) }
        val mean = Stats.weightedMean(weightsAndMean)
        val std = Stats.weightedStandardDeviation(weightsAndMean)
        return MeanAndStd(mean, std)
    }

    fun display(): String {
        var str = "Town $name - mean(${stats().mean} std(${stats().std})\n"
        schools.forEach {
            str += it.display()
        }
        return str
    }
}

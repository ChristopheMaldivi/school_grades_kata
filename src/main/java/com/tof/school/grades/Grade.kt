package com.tof.school.grades

data class Grade(val grade: Int) : StatValue {
    override fun value() = grade
}

package com.tof.school.grades

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class TownTest {
    @Test
    fun `weighted mean tree`() {
        // when
        val str =
            Town(
                "Paris",
                School(
                    "Fake Carnot",
                    ClassRoom("A", Grade(10), Grade(15), Grade(18)),
                    ClassRoom("JD 🧬", Grade(8), Grade(8), Grade(10), Grade(12), Grade(12)),
                    ClassRoom("C", Grade(10), Grade(15), Grade(18))
                ),
                School(
                    "True Carnot",
                    ClassRoom("JG 🏍️", Grade(18)),
                    ClassRoom("2", Grade(16), Grade(15), Grade(18), Grade(20))
                )
            ).display()

        // then
        // classroom 2
        then(Stats.mean(listOf(Grade(16), Grade(15), Grade(18), Grade(20)))).isEqualTo(17.25)
        then(Stats.standardDeviation(listOf(Grade(16), Grade(15), Grade(18), Grade(20)))).isEqualTo(1.920286436967152)
        // school True Carnot
        then(Stats.weightedMean(listOf(Pair(1, 18.0), Pair(4, 17.25)))).isEqualTo(17.4)
        then(Stats.weightedStandardDeviation(listOf(Pair(1, 18.0), Pair(4, 17.25)))).isEqualTo(0.3)
        // school Fake Carnot
        then(Stats.weightedMean(listOf(Pair(3, 14.333333333333334), Pair(5, 10.0), Pair(3, 14.333333333333334)))).isEqualTo(12.363636363636363)
        then(Stats.weightedStandardDeviation(listOf(Pair(3, 14.333333333333334), Pair(5, 10.0), Pair(3, 14.333333333333334)))).isEqualTo(2.1576949235052)
        // town
        then(Stats.weightedMean(listOf(Pair(3, 12.363636363636363), Pair(2, 17.4)))).isEqualTo(14.378181818181819)
        then(Stats.weightedStandardDeviation(listOf(Pair(3, 12.363636363636363), Pair(2, 17.4)))).isEqualTo(2.4673042136397823)

        println(str)

        then(str).isEqualTo(
            """Town Paris - mean(14.378181818181819 std(2.4673042136397823)
	School Fake Carnot - mean(12.363636363636363 std(2.1576949235052)
		ClassRoom A - mean(14.333333333333334 std(3.2998316455372216)
		ClassRoom JD 🧬 - mean(10.0 std(1.7888543819998317)
		ClassRoom C - mean(14.333333333333334 std(3.2998316455372216)
	School True Carnot - mean(17.4 std(0.3)
		ClassRoom JG 🏍️ - mean(18.0 std(0.0)
		ClassRoom 2 - mean(17.25 std(1.920286436967152)
"""
        )
    }
}

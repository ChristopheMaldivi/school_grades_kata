package com.tof.school.grades

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class SchoolStatsTest {

    private val classRoom1With3Grades = listOf(
        Grade(10),
        Grade(15),
        Grade(18),
    )

    private val classRoom2With5Grades = listOf(
        Grade(8),
        Grade(8),
        Grade(10),
        Grade(12),
        Grade(12),
    )

    @Test
    fun `mean for a classroom`() {
        then(GradesStats.mean(classRoom1With3Grades)).isEqualTo(14.333333333333334)
        then(GradesStats.mean(classRoom2With5Grades)).isEqualTo(10.0)
    }

    @Test
    fun `standard deviation for a classroom`() {
        then(GradesStats.standardDeviation(classRoom1With3Grades)).isEqualTo(3.2998316455372216)
        then(GradesStats.standardDeviation(classRoom2With5Grades)).isEqualTo(1.7888543819998317)
    }

    @Test
    fun `weighted mean of two classrooms`() {
        then(GradesStats.weightedMean(listOf(classRoom1With3Grades, classRoom2With5Grades))).isEqualTo(11.625)
        then(Stats.weightedMean(listOf(Pair(3, 14.333333333333334), Pair(5, 10.0)))).isEqualTo(11.625)
    }

    @Test
    fun `weighted standard deviation of two classrooms`() {
        then(GradesStats.weightedStandardDeviation(listOf(classRoom1With3Grades, classRoom2With5Grades))).isEqualTo(2.0978659791956846)
        then(Stats.weightedStandardDeviation(listOf(Pair(3, 14.333333333333334), Pair(5, 10.0)))).isEqualTo(2.0978659791956846)
    }
}
